package main

type Exercise struct {
	ID       int    `json:"id"`
	Name     string `json:"name"`
	WorkSets []int  `json:"worksets"`
}

type Routine struct {
	ID        int        `json:"id" storm:"id,increment"`
	Day       string     `json:"day"`
	Exercises []Exercise `json:"exercises"`
}
