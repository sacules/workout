package main

import (
	"context"
	"log"

	"github.com/asdine/storm/v3"
)

// App struct
type App struct {
	ctx context.Context
	db  *storm.DB
}

// NewApp creates a new App application struct
func NewApp() *App {
	return &App{}
}

// startup is called when the app starts. The context is saved
// so we can call the runtime methods
func (a *App) startup(ctx context.Context) {
	a.ctx = ctx

	db, err := storm.Open("workout.db")
	if err != nil {
		log.Fatalf("couldn't open the database: %v", err)
	}

	db.Init(&Routine{})

	a.db = db
}

func (a *App) shutdown(ctx context.Context) {
	a.db.Close()
}

func (a *App) NewRoutine() Routine {
	r := Routine{
		Exercises: []Exercise{
			{Name: "", WorkSets: make([]int, 0)},
		},
	}

	return r
}

func (a *App) GetRoutines() ([]Routine, error) {
	var r []Routine

	err := a.db.All(&r)
	if err != nil {
		log.Printf("couldn't find the routines: %v", err)

		return r, err
	}

	return r, nil
}

func (a *App) SetRoutine(r Routine) {
	err := a.db.Save(&r)
	if err != nil {
		log.Printf("couldn't save routine: %v", err)
	}
}

func (a *App) DeleteRoutine(r Routine) {
	a.db.DeleteStruct(&r)
}
