import React, { ReactNode, useEffect, useState } from "react";

// Go methods
import { GetRoutines } from "../wailsjs/go/main/App";

// Go models
import { main } from "../wailsjs/go/models";

// Pages
import Default from "./pages/Default";

// Styles
import "./App.css";

interface Props {
  children: ReactNode;
}

const Main: React.FC<Props> = ({ children }) => (
  <main className="bg-zinc-800 text-zinc-50 h-screen">{children}</main>
);

function App() {
  const [loading, setLoading] = useState(true);
  const [routines, setRoutines] = useState<main.Routine[]>([]);
  const [error, setError] = useState<Error>();

  const fetchRoutines = () => {
    GetRoutines()
      .then((r) => setRoutines(r as main.Routine[]))
      .catch((err) => setError(err));
  };

  useEffect(() => {
    fetchRoutines();
    setLoading(false);
  }, []);

  if (loading) {
    return <Main>Loading...</Main>;
  }

  if (error) {
    return <Main>Error: {error.message}</Main>;
  }

  return (
    <Main>
      {routines && (
        <Default routines={routines} fetchRoutines={fetchRoutines} />
      )}
    </Main>
  );
}

export default App;
