import React from "react";

interface Props {
  date: Date;
}

const Header: React.FC<Props> = ({ date }) => (
  <header>
    <p>{date.toLocaleDateString("es-AR", { weekday: "long" })}</p>
    <p>{`${date.getDay()}/${date.getMonth()}/${date.getFullYear()}`}</p>
  </header>
);

export default Header;
