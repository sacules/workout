import React from "react";

// Go models
import { main } from "../../wailsjs/go/models";

interface Props {
  value: main.Exercise;
  index: number;
  setExercises: React.Dispatch<React.SetStateAction<main.Exercise[]>>;
}

const Exercise: React.FC<Props> = ({ value, index, setExercises }) => {
  const setName = (ev: React.ChangeEvent<HTMLInputElement>) =>
    setExercises((ex) => {
      const newEx = [...ex];
      newEx[index].name = ev.target.value;

      return newEx;
    });

  const setRep = (ev: React.ChangeEvent<HTMLInputElement>, repIndex: number) =>
    setExercises((ex) => {
      const newEx = [...ex];
      newEx[index].worksets[repIndex] = Number(ev.target.value);

      return newEx;
    });

  const addRep = () => {
    setExercises((ex) => {
      const newEx = JSON.parse(JSON.stringify(ex));
      newEx[index].worksets = [...newEx[index].worksets, 0];

      return newEx;
    });
  };

  const removeRep = () =>
    setExercises((ex) => {
      const newEx = JSON.parse(JSON.stringify(ex));
      newEx[index].worksets.pop();

      return newEx;
    });

  return (
    <div className="flex nowrap gap-2 items-center group px-8 py-4 transition">
      <input
        className="border-b-2 focus:border-sky-500 transition bg-transparent"
        placeholder="Name"
        value={value.name}
        onChange={setName}
      />
      <ul className="flex nowrap gap-4">
        {value.worksets.map((w, i) => (
          <li>
            <input
              className="bg-transparent border-b-2 focus:border-sky-500 transition"
              type="number"
              placeholder="Name"
              value={w}
              onChange={(ev) => setRep(ev, i)}
              min="0"
              max="99"
            />
          </li>
        ))}
      </ul>
      <button
        className="text-xl transition opacity-0 group-hover:opacity-100 w-8 h-8 hover:bg-green-500 rounded-full"
        onClick={addRep}
      >
        +
      </button>
      {value.worksets.length > 0 && (
        <button
          className="text-xl transition-colors invisible group-hover:visible w-8 h-8 hover:bg-red-500 rounded-full"
          onClick={removeRep}
        >
          -
        </button>
      )}
    </div>
  );
};

export default Exercise;
