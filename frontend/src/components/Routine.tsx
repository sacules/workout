import React, { useEffect, useState } from "react";

// Go models
import { main } from "../../wailsjs/go/models";

// Go methods
import { SetRoutine } from "../../wailsjs/go/main/App";

// Components
import Exercise from "../components/Exercise";

interface Props {
  value: main.Routine;
}

const Routine: React.FC<Props> = ({ value }) => {
  const [exercises, setExercises] = useState<main.Exercise[]>([
    ...value.exercises,
  ]);

  useEffect(() => {
    value.exercises = exercises;
    SetRoutine(value);
  }, [exercises]);

  const addExercise = () => {
    const e = new main.Exercise();
    e.worksets = [0];
    setExercises([...exercises, e]);
  };

  const removeExercise = (i: number) =>
    setExercises((ex) => {
      const newEx = [...ex];
      newEx.splice(i, 1);

      return newEx;
    });

  const editDay = (ev: React.ChangeEvent<HTMLInputElement>) => {
    value.day = ev.target.value;
    SetRoutine(value);
  };

  return (
    <div className="w-full h-full grid place-items-center rounded px-4 py-2">
      <div className="flex justify-between w-full group">
        <input
          className="text-zinc-300 bg-transparent focus:border-b-2 transition"
          placeholder="Day"
          value={value.day}
          onChange={editDay}
        />
        <button
          className="invisible group-hover:visible hover:bg-green-500 rounded-full w-8 h-8 transition"
          onClick={addExercise}
        >
          +
        </button>
      </div>
      <ul className="flex flex-col gap-4">
        {exercises.map((e, i) => (
          <li key={i} className="flex items-center nowrap group gap-4">
            <Exercise value={e} index={i} setExercises={setExercises} />
            <button
              className="invisible group-hover:visible hover:bg-red-500 rounded-full w-8 h-8"
              onClick={() => removeExercise(i)}
            >
              x
            </button>
          </li>
        ))}
      </ul>
    </div>
  );
};

export default Routine;
