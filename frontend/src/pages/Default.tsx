import React from "react";

// Go models
import { main } from "../../wailsjs/go/models";

// Go methods
import {
  NewRoutine,
  GetRoutines,
  SetRoutine,
  DeleteRoutine,
} from "../../wailsjs/go/main/App";

// Components
import Routine from "../components/Routine";

interface Props {
  routines: main.Routine[];
  fetchRoutines: () => void;
}

const Default: React.FC<Props> = ({ routines, fetchRoutines }) => {
  const addRoutine = async () => {
    const r = await NewRoutine();
    SetRoutine(r);
    fetchRoutines();
  };

  const removeRoutine = (r: main.Routine) => DeleteRoutine(r);

  return (
    <div className="px-4 py-2 grid place-items-center h-full">
      {routines.length > 0 && (
        <ul>
          {routines.map((r) => (
            <div className="group flex items-center">
              <Routine value={r} />
              <button
                className="invisible group-hover:visible hover:bg-red-500 rounded-full w-8 h-8"
                onClick={() => removeRoutine(r)}
              >
                x
              </button>
            </div>
          ))}
        </ul>
      )}
      <button
        className="text-5xl hover:bg-green-500 bg-zinc-700 rounded-full w-12 h-12"
        onClick={addRoutine}
      >
        +
      </button>
    </div>
  );
};

export default Default;
